import pyotb

xs_path = 'xs.tif'
pan_path = 'pan.vrt'
cloud_path = 'cloud_mask.GML'

# Pansharpening
pxs = pyotb.BundleToPerfectSensor(inp=pan_path, inxs=xs_path, method='rcs') 
pxs.write('pxs.tif')


cloud_mask = pyotb.Rasterization(cloud_path, im=pan_path)
